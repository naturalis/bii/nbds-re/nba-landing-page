# nba-html-pages

Landing and documentation pages for the Naturalis Biodiversity API.

(reverse engineered from https://hub.docker.com/repository/docker/naturalis/nba-landing)


## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* `pre-commit autoupdate`
* `pre-commit install`

