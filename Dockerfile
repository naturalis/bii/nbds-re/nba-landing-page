FROM nginx:1.18

COPY nginx.conf /etc/nginx/nginx.conf
COPY default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80/tcp

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]

COPY html/index.html /usr/share/nginx/html/index.html